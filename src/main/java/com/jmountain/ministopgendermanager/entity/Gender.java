package com.jmountain.ministopgendermanager.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Gender {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)//nullable = false 는 빈공간을 가능하게 하지 않겠다
    private String productName;
    @Column(nullable = false, length = 20)
    private String isMan;
    @Column(nullable = false, length = 20)
    private String isWoman;
    @Column(nullable = false, length = 20)
    private int theAgeGroup;
}

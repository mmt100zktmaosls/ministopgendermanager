package com.jmountain.ministopgendermanager.service;

import com.jmountain.ministopgendermanager.entity.Gender;
import com.jmountain.ministopgendermanager.repository.GenderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GenderService {
    private final GenderRepository genderRepository;

    public void setGender(String productName, String isMan, String isWoman, int theAgeGroup) {
        Gender addData = new Gender();
        addData.setIsMan(isMan);
        addData.setIsWoman(isWoman);
        addData.setProductName(productName);

        genderRepository.save(addData);

    }
}

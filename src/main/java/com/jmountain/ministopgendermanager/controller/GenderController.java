package com.jmountain.ministopgendermanager.controller;

import com.jmountain.ministopgendermanager.entity.Gender;
import com.jmountain.ministopgendermanager.model.GenderRequest;
import com.jmountain.ministopgendermanager.service.GenderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/gender")
public class GenderController {
    private final GenderService genderService;
@RequestMapping("/data")
    public String setGender(@RequestBody GenderRequest request) {
        genderService.setGender(request.getProductName(), request.getIsMan(), request.getIsWoman(), request.getTheAgeGroup());

            return "OK";
    }
}
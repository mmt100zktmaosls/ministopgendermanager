package com.jmountain.ministopgendermanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenderRequest {
    private String isMan;
    private String isWoman;
    private String productName;
    private int theAgeGroup;
}

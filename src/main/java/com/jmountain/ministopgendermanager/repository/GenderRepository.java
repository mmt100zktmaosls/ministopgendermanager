package com.jmountain.ministopgendermanager.repository;

import com.jmountain.ministopgendermanager.entity.Gender;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenderRepository extends JpaRepository<Gender, Long> {
}

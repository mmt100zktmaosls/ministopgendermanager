package com.jmountain.ministopgendermanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinistopGenderManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinistopGenderManagerApplication.class, args);
    }

}
